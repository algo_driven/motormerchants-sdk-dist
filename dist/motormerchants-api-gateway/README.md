# MotorMerchants API Gateway
## Overview
This API Gateway allows you to integrate https://motormerchants.com/ logic in your backend, it's never recommended to connect to this gateway directly from your frontend you should it only call it from your backend

## How to Use
- You need a server that has nodejs 14+ installed
- make sure to download all files from `motormerchants-api-gateway` to your server
- create `config/json` with the following content

```js
 {
    "mode": "dev", //dev or prod
    "apiKey": "API_KEY", // api key provided to you by AlgoDriven
    "country": "AE",
    "port": 3002, //that port were API Gateway will run
    "authKey": "AUTH_KEY" // auth key you generate, it's used to authorize all the endpoints in the API Gateway
}

```


## Endpoints
For complete list of endpoints with sample requests/responses please refer to this Postman Documentation
https://documenter.getpostman.com/view/16618966/UV5TEeiC

### /createEvalFromVehicle
`multipart/form-data`

This endpoint can decode VIN/Chassis Number, Australian Registration, ADcode or UAE Mulkiya and initiate an evaluation out of it
### /getLookupListings (UAE ONLY)
`application/json`

If the user wants to manually pickup a vehicle, this functions will give you all needed listings till you get an ADcode that you can send to `/createEvalFromVehicle`, the type of lists it returns depends on the sent arguments, the response is always in this format:
```ts
 {
     type: string //type of the list,ex: yeas,makes,models...etc
     list: [ //actual list
         {
             label: string
             value: string
         }
     ],
     adcode: string //returned only if the selected filters matches a single vehicle
 }
```

Here are post bodies of how to get all different list types
```js
//Years
{}

//Makes
{"selectedYear":"2020"}

//Models
{"selectedYear":"2020","selectedMake":"NI"}

//Variants
{"selectedYear":"2020","selectedMake":"NI","selectedModel":"SUN"}

//Bodies
{"selectedYear":"2020","selectedMake":"NI","selectedModel":"SUN","selectedVariant":"S"}

//Transmissions
{"selectedYear":"2020","selectedMake":"NI","selectedModel":"SUN","selectedVariant":"S","selectedBody":"Sedan"}

//Litres
{"selectedYear":"2020","selectedMake":"NI","selectedModel":"SUN","selectedVariant":"S","selectedBody":"Sedan","selectedTransmission":"Automatic"}

//Cylinders
{"selectedYear":"2020","selectedMake":"NI","selectedModel":"SUN","selectedVariant":"S","selectedBody":"Sedan","selectedTransmission":"Automatic","selectedLitres":"1.6"}

//Drivetrains
{"selectedYear":"2020","selectedMake":"NI","selectedModel":"SUN","selectedVariant":"S","selectedBody":"Sedan","selectedTransmission":"Automatic","selectedLitres":"1.6","selectedCylinders":"4"}

//Descriptions
{"selectedYear":"2020","selectedMake":"NI","selectedModel":"SUN","selectedVariant":"S","selectedBody":"Sedan","selectedTransmission":"Automatic","selectedLitres":"1.6","selectedCylinders":"4","selectedDrivetrain":"Front Wheel Drive"}


```

### /uploadImage
`multipart/form-data`

Uploads vehicle image to a pre created Eval

### /getConditionOptions
`application/json`

Gets list of possible vehicle conditions for user to select from the list,

### /getSpareKeyOptions
`application/json`

Gets list of possible vehicle spare key options for user to select from the list,


### /getServiceHistoryOptions
`application/json`

Gets list of possible service history options for user to select from the list,
### /submitEval
`application/json`

Submits Evaluation to MotorMerchants where it's shared with dealers and sends Email/SMS to user with his offers link
