# MotorMerchants Web SDK
## Overview
This SDK allows you integrate https://motormerchants.com/ logic in your website,

## How to Use
add the following snippet to your html page
```html
    <script src="./motormerchants-sdk.js"></script>
    <script>
        window.MotorMerchants.init({
            mode: 'dev' // dev or prod
            apiKey: 'API_KEY_HERE' // api key provided to you
            country: 'AE' // AE for United Arab Emirates or AU for Australia
        }).then(() => {
            console.log('motormerchants intergration is ready')
        }).catch(e) => {
            console.error(e)

        } 
    </script>

```


## Functions

### createEvalFromVehicle()
This functions can decode VIN/Chassis Number,Australian Registration, ADcode or UAE Mulkiya and initiate an evaluation out of it, example use:
```js
//By VIN/Chassis Number
const chassisEval =   await window.MotorMerchants.createEvalFromVehicle({
            type: 'chassis',
            value: "WAU9FDFR1FA000284"
    });
//By ADCode, UAE ONLY
const adCodeEval =   await window.MotorMerchants.createEvalFromVehicle({
            type: 'adcode',
            value: "AENISUN12AAAA"
    });
//By Mulkiya , UAE ONLY
const mulkiyaEval = await window.MotorMerchants.createEvalFromVehicle({
            type: 'mulkiya',
            value:  document.querySelector('#fileInput').files[0] 
            // from <input type='file' id='fileInput' />
     });

//By  Registration, Australia ONLY
const registrationEval = await window.MotorMerchants.createEvalFromVehicle({
            type: 'registration',
            value:  'SA',
            state: 'WA', 
     });

```

Sample Response 
```js
 {
    Built: "2020"
    Compliance: "2020"
    Description: "2020 Nissan Sunny S (N18), 4dr Sedan, 1.6L 4cyl Petrol, Automatic, Front Wheel Drive"
    Engine: "Petrol,4 Cylinders, 1.6L  "
    EvalID: 2041635
    Transmission: "Automatic"
    VinID: "EE-1634029066-10248-95"

 }

```
### getLookupListings()  (UAE ONLY)
If the user wants to manually pickup a vehicle, this functions will give you all needed listings till you get an ADcode that you can send to `createEvalFromVehicle`, the type of lists it returns depends on the sent arguments, the response is always in this format:
```ts
 {
     type: string //type of the list,ex: yeas,makes,models...etc
     list: [ //actual list
         {
             label: string
             value: string
         }
     ],
     adcode: string //returned only if the selected filters matches a single vehicle
 }
```

Here are examples of how to get all different list types based on the sent arguments
```js
const years = await window.MotorMerchants.getLookupListings();

const makes = await window.MotorMerchants.getLookupListings({ selectedYear: '2020' });

const models = await window.MotorMerchants.getLookupListings({ selectedYear: '2020', selectedMake: 'NI' });

const variants = await window.MotorMerchants.getLookupListings({ selectedYear: '2020', selectedMake: 'NI', selectedModel: 'SUN' });

const bodies = await window.MotorMerchants.getLookupListings({ selectedYear: '2020', selectedMake: 'NI', selectedModel: 'SUN', selectedVariant: 'S' });

const transmissions = await window.MotorMerchants.getLookupListings({ selectedYear: '2020', selectedMake: 'NI', selectedModel: 'SUN', selectedVariant: 'S', selectedBody: 'Sedan' });

const litres = await window.MotorMerchants.getLookupListings({ selectedYear: '2020', selectedMake: 'NI', selectedModel: 'SUN', selectedVariant: 'S', selectedBody: 'Sedan', selectedTransmission: 'Automatic' });

const cylinders = await window.MotorMerchants.getLookupListings({ selectedYear: '2020', selectedMake: 'NI', selectedModel: 'SUN', selectedVariant: 'S', selectedBody: 'Sedan', selectedTransmission: 'Automatic', selectedLitres: '1.6' });

const drivetrains = await window.MotorMerchants.getLookupListings({ selectedYear: '2020', selectedMake: 'NI', selectedModel: 'SUN', selectedVariant: 'S', selectedBody: 'Sedan', selectedTransmission: 'Automatic', selectedLitres: '1.6', selectedCylinders: '4' });

const descriptions = await window.MotorMerchants.getLookupListings({ selectedYear: '2020', selectedMake: 'NI', selectedModel: 'SUN', selectedVariant: 'S', selectedBody: 'Sedan', selectedTransmission: 'Automatic', selectedLitres: '1.6', selectedCylinders: '4', selectedDrivetrain: 'Front Wheel Drive' });


```

### uploadImage()
Uploads vehicle image to a pre created Eval, it takes arguments of `evalId` and `file`

example use
```js
 const file =  document.querySelector('#fileInput').files[0] 
            // from <input type='file' id='fileInput' />
 await window.MotorMerchants.uploadImage('2041635', file);

```

### getConditionOptions()
Gets list of possible vehicle conditions for user to select from the list,
```js
const conditions = window.MotorMerchants.getConditionOptions()

/*
Returns
[{"value":"0","label":"Unroadworthy"},{"value":"1","label":"1"},{"value":"2","label":"2"},{"value":"3","label":"3"},{"value":"4","label":"4"},{"value":"5","label":"Average"},{"value":"6","label":"6"},{"value":"7","label":"7"},{"value":"8","label":"8"},{"value":"9","label":"9"},{"value":"10","label":"Perfect"}]
*/
```

### getSpareKeyOptions()
Gets list of possible vehicle spare key options for user to select from the list,
```js
const spareKeys = window.MotorMerchants.getSpareKeyOptions()

/*
Returns
[
    {
        "value": "0",
        "label": "The vehicle has main key only"
    },
    {
        "value": "1",
        "label": "The vehicle has main key and a spare key"
    }
]
*/
```


### getServiceHistoryOptions()
Gets list of possible service history options for user to select from the list,
```js
const serviceHistories = window.MotorMerchants.getServiceHistoryOptions()

/*
Returns
[
    {
        "value": "5",
        "label": "Full genuine history"
    },
    {
        "value": "4",
        "label": "Full non-genuine history"
    },
    {
        "value": "3",
        "label": "Partial history"
    },
    {
        "value": "2",
        "label": "Missing book / no history"
    }
]
*/
```

### submitEval()
Submits Evaluation to MotorMerchants where it's shared with dealers and sends Email/SMS to user with his offers link

```js
  await window.MotorMerchants.submitEval({
            evalId:'2041635',
            vehicleInfo: {
                condition: '2', //selected from results of getConditionOptions()
                km: '35000', // car Mileage
                notes: 'back bumper small damage', //notes input by user
                warning: true, // boolean input by user if vehicle dashboard has any warnings
                spareKey: '1', //selected from results of getSpareKeyOptions()
                service: '4', //selected from results of getServiceHistoryOptions()

            },

            customerInfo: {
                name: 'Walid Abou Ali',
                email: 'walid.abouali@algodriven.xyz',
                suburb: '', // Not needed for AE, only required for AU
                postal: '1234',// Not needed for AE, only required for AU
                state: 'Dubai',
                phone: '0509322583'
            }
        },

        )

    });
```
